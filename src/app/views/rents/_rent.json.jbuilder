json.extract! rent, :id, :user_id, :movie_id, :date_from, :date_to, :status, :created_at, :updated_at
json.url rent_url(rent, format: :json)
