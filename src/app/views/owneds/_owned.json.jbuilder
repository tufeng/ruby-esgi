json.extract! owned, :id, :movie_id, :user_id, :date_add, :availability, :created_at, :updated_at
json.url owned_url(owned, format: :json)
