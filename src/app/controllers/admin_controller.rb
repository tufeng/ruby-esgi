class AdminController < ApplicationController

  def index
    authorize! :read, Admin
  end

  def show
  end

  def dashboard
  end
end
