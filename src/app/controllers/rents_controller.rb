class RentsController < ApplicationController
  before_action :set_rent, only: [:show, :edit, :update, :destroy]
  # GET /rents/1
  # GET /rents/1.json
  def index
    @rents = Rent.all
  end
  # GET /rents/1
  # GET /rents/1.json
  def show
  end

  # GET /rents/new
  def new
    @rent = Rent.new
  end

  # GET /rents/1/edit
  def edit
  end

  # POST /rents
  # POST /rents.json
  def create
    @rent = Rent.new
    @rent.movie_id = params[:movie_id]
    @rent.user_id = current_user.id
    @rent.date_from = Date.today
    @rent.date_to = Date.today + 30

    puts "ornage"

    respond_to do |format|
      if @rent.save
        format.html { redirect_to request.env['HTTP_REFERER'], notice: 'Rent was successfully added.' }
      else
        format.html { redirect_to request.env['HTTP_REFERER'], notice: 'Rent was not added.' }
      end
    end
  end

  # PATCH/PUT /rents/1
  # PATCH/PUT /rents/1.json
  def update
    respond_to do |format|
      if @rent.update(rent_params)
        format.html { redirect_to @rent, notice: 'Rent was successfully updated.' }
        format.json { render :show, status: :ok, location: @rent }
      else
        format.html { render :edit }
        format.json { render json: @rent.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /rents/1
  # DELETE /rents/1.json
  def destroy
    @rent.destroy
    respond_to do |format|
      format.html { redirect_to rents_url, notice: 'Rent was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_rent
      @rent = Rent.find(params[:id])
    end
end
