class Admin::DashboardController < ApplicationController
  layout 'admin'

  def dashboard
    authorize! :manage, Admin
    @users = User.all
    @rents = Rent.all
  end
end