class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :configure_permitted_parameters,
                #:authenticate_user! only: [:movies],
                if: :devise_controller?

  layout 'admin'

  protected


  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:username,:email])
    devise_parameter_sanitizer.permit(:sign_in, keys: [:login])
  end

  # Pour récupérer une erreur cancan et faire une action particulière, ici la redirection avec un message de notification

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to new_user_session_path, notify: 'Accès interdit'
  end
end