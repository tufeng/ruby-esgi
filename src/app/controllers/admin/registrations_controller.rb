class Admin::RegistrationsController < Devise::RegistrationsController
  layout 'admin'

  def new
    authorize! :manage, Admin
  end

  def edit
    authorize! :manage, Admin
  end

  def destroy
    authorize! :manage, Admin
  end
end
