class Admin::ListController < ApplicationController
  layout 'admin'

  def users_list
    authorize! :manage, Admin
    @users = User.all
  end

  def movies_list
    authorize! :manage, Admin
    @movies = Movie.all
  end
  def roles_list
    authorize! :manage, Admin
    @roles = Role.all
  end
end