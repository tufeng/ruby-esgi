class OwnedsController < ApplicationController
  before_action :set_owned, only: [:show, :edit, :update, :destroy]

  # GET /owneds
  # GET /owneds.json
  def index
    @owneds = Owned.all
  end

  # GET /owneds/1
  # GET /owneds/1.json
  def show
  end

  # GET /owneds/new
  def new
    @owned = Owned.new
  end

  # POST /owneds
  # POST /owneds.json
  def create
    @owned = Owned.new
    @owned.user_id = current_user.id
    @owned.movie_id = params[:movie_id]
    @owned.date_add = Date.today
    @owned.availability = true

    respond_to do |format|
      if @owned.save
        format.html { redirect_to request.env['HTTP_REFERER'], notice: 'Owned was successfully added.' }
      else
        format.html { redirect_to request.env['HTTP_REFERER'], notice: 'Owned was not added.' }
      end
    end
  end

  # PATCH/PUT /owneds/1
  # PATCH/PUT /owneds/1.json
  def update
    respond_to do |format|
      if @owned.update(owned_params)
        format.html { redirect_to @owned, notice: 'Owned was successfully updated.' }
        format.json { render :show, status: :ok, location: @owned }
      else
        format.html { render :edit }
        format.json { render json: @owned.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /owneds/1
  # DELETE /owneds/1.json
  def destroy
    @owned.destroy
    respond_to do |format|
      format.html { redirect_to owneds_url, notice: 'Owned was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_owned
      @owned = Owned.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def owned_params
      params.require(:owned).permit(:movie_id, :user_id, :date_add, :availability)
    end
end
