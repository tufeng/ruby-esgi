class MoviesController < ApplicationController
  before_action :set_movie, only: [:show, :edit, :update, :destroy, :library]
  authorize_resource
  # load_and_authorize_resource

  # GET /movies
  # GET /movies.json
  def index
    @movies = Movie.all.order("created_at DESC")
  end

  def repository
    @movies = Movie.all
    render :index
  end

  # GET /movies/1
  # GET /movies/1.json
  def show
  end

  # GET /movies/new
  def new
    @movie = Movie.new
  end

  # GET /movies/1/edit
  def edit
  end

  # POST /movies
  # POST /movies.json
  def create
    @movie = current_user.movies.new(movie_params)


    respond_to do |format|
      if @movie.save
        format.html { redirect_to @movie, notice: 'Movie was successfully created.' }
        format.json { render :show, status: :created, location: @movie }
      else
        format.html { render :new }
        format.json { render json: @movie.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /movies/1
  # PATCH/PUT /movies/1.json
  def update
    respond_to do |format|
      if @movie.update(movie_params)
        format.html { redirect_to @movie, notice: 'Movie was successfully updated.' }
        format.json { render :show, status: :ok, location: @movie }
      else
        format.html { render :edit }
        format.json { render json: @movie.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /movies/1
  # DELETE /movies/1.json
  def destroy
    @movie.destroy
    redirect_to root_path
  end

  #add and remove movies from a library
  def library
    type = params[:type]

    if type == "add"
      current_user.library_additions << @movie
      redirect_to library_index_path, notice: '#{@movie.title} was added to your library'
    elsif type == "remove"
      current_user.library_additions.delete(@movie)
      redirect_to root_path, notice: '#{@movie.title} was removed from your library'
    else
      redirect_to root_path
    end

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_movie
      @movie = Movie.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def movie_params
      params.require(:movie).permit(:name, :description, :category, :image, :thumbnail)
    end
end
