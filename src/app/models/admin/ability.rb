# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new

    # # création d'un alias
    alias_action :create,:show,:update,:destroy, to: :crud
    # # Utilisation de l'alias avec l'access à repository
    # can [:crud, :repository], Movie, {user_id: :user.id}
    # # L'inverse de can, refus d'acces
    # cannot? [:crud, :repository], Movie, {user_id: :user.id}

    # Pour donner accès au lecture aux utilisateurs connecté
    can [:read], Movie

    if user.id && user.role.name === "member"
      can [:crud], Movie, {user_id: user.id}
      cannot :manage, Dashboard
    end


    if user.id && user.role.name === "admin"
      can :manage, Movie
      can :manage, Role
      can :manage, Admin
    end

    # can [:create,:show, :update], Movie, {user_id: :user.id}
    # can :manage, Movie, {user_id: user.id}

    # Pour restreindre l'acces si il ya un role admin par exemple
    # if user.admin?
    #  can [:crud], :all # ou [Movie,Post,...]
    # end

    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end
