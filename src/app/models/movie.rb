class Movie < ApplicationRecord
  belongs_to :user
  has_many :libraries
  has_many :added_movies, through: :libraries, source: :user
  has_many :reviews

  has_one_attached :image
end
