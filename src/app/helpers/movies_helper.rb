module MoviesHelper

    def user_add_to_library? user, movie 
        user.libraries.where(user: user, movie: movie).any?
    end
end
