class CreateRents < ActiveRecord::Migration[6.0]
  def change
    create_table :rents do |t|
      t.references :user, null: false, foreign_key: true
      t.references :movie, null: false, foreign_key: true
      t.date :date_from
      t.date :date_to
      t.boolean :status
      t.boolean :seen

      t.timestamps
    end
  end
end
