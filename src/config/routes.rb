Rails.application.routes.draw do

  resources :owneds
  resources :rents
  resources :roles
  resources :movies do
    resources :reviews
  end

  devise_for :users

  resources :admin

  scope 'admin', module: 'admin', as: 'admin' do
    devise_for :users
    resources :movies
    resources :roles

    ## Liste de donnée
    get 'list/users', to: 'list#users_list' # Liste Utilisateur
    get 'list/movies', to: 'list#movies_list' # Liste Film
    get 'list/roles', to: 'list#roles_list' # Liste Roles
    # Liste Commentaire
    # Liste Location
    # Liste Like
    # Liste Ajout
    # Liste possède + disponibilité

    ## Dashboard
    get 'dashboard/index', to: 'dashboard#dashboard'
  end

  resources :library
  root 'movies#index'

  get 'home/private'

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  get 'browse' => 'browse#browse', as: :browse
end
