require 'test_helper'

class OwnedsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @owned = owneds(:one)
  end

  test "should get index" do
    get owneds_url
    assert_response :success
  end

  test "should get new" do
    get new_owned_url
    assert_response :success
  end

  test "should create owned" do
    assert_difference('Owned.count') do
      post owneds_url, params: { owned: { availability: @owned.availability, date_add: @owned.date_add, movie_id: @owned.movie_id, user_id: @owned.user_id } }
    end

    assert_redirected_to owned_url(Owned.last)
  end

  test "should show owned" do
    get owned_url(@owned)
    assert_response :success
  end

  test "should get edit" do
    get edit_owned_url(@owned)
    assert_response :success
  end

  test "should update owned" do
    patch owned_url(@owned), params: { owned: { availability: @owned.availability, date_add: @owned.date_add, movie_id: @owned.movie_id, user_id: @owned.user_id } }
    assert_redirected_to owned_url(@owned)
  end

  test "should destroy owned" do
    assert_difference('Owned.count', -1) do
      delete owned_url(@owned)
    end

    assert_redirected_to owneds_url
  end
end
