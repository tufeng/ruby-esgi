require "application_system_test_case"

class OwnedsTest < ApplicationSystemTestCase
  setup do
    @owned = owneds(:one)
  end

  test "visiting the index" do
    visit owneds_url
    assert_selector "h1", text: "Owneds"
  end

  test "creating a Owned" do
    visit owneds_url
    click_on "New Owned"

    fill_in "Availability", with: @owned.availability
    fill_in "Date add", with: @owned.date_add
    fill_in "Movie", with: @owned.movie_id
    fill_in "User", with: @owned.user_id
    click_on "Create Owned"

    assert_text "Owned was successfully created"
    click_on "Back"
  end

  test "updating a Owned" do
    visit owneds_url
    click_on "Edit", match: :first

    fill_in "Availability", with: @owned.availability
    fill_in "Date add", with: @owned.date_add
    fill_in "Movie", with: @owned.movie_id
    fill_in "User", with: @owned.user_id
    click_on "Update Owned"

    assert_text "Owned was successfully updated"
    click_on "Back"
  end

  test "destroying a Owned" do
    visit owneds_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Owned was successfully destroyed"
  end
end
