## USER ACCOUNT 
login : orange
mdp : orange

## commande à lancer
```
bundle install
yarn install --check-files
rails g webpacker:install
```


## SQL pour ajouter un user
```

INSERT INTO roles(id,name,created_at,updated_at) VALUES(1,'admin','20200628','20200628');  


 INSERT INTO `users` (`id`, `email`, `encrypted_password`, `username`, `reset_password_token`, `reset_password_sent_at`, `remember_created_at`, `sign_in_count`, `current_sign_in_at`, `last_sign_in_at`, `current_sign_in_ip`, `last_sign_in_ip`, `confirmation_token`, `confirmed_at`, `confirmation_sent_at`, `unconfirmed_email`, `created_at`, `updated_at`, `role_id`)
VALUES
    (1,'orange@orange.fr','$2a$11$WpT6NOtmUoxJM0VGIPSqaOH1VaXikcoqcqFXMV2K/RUfiXcyBs7q6','orange',NULL,NULL,NULL,1,'2020-06-23 11:12:40','2020-06-23 11:12:40','192.168.128.1','192.168.128.1','zzW2vsr17JZNW4jajuNt','2020-06-23 11:12:34','2020-06-23 11:12:21',NULL,'2020-06-23 11:12:21.702429','2020-06-23 11:12:40.738054',1);
```

## Branche git pour les opérations CRUD sur les filmes et les commentaires  :
```
imp_movies_feat

```

## Docker-compose
```
Deux docker-compose sont mit à disposition. Pour s'en servir :
raname docker-compose.yml.dist to docker-compose.yml
Ou bien 
raname docker-compose.yml.sabrina to docker-compose.yml